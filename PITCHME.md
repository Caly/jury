### Refactoring et extension de la plateforme web de réservation de navettes de proximité de SAM-Drive

---

### Présentation personnelle

* Étudiant à la HELHa
* Autodidacte UI et développement informatique
* Contributeur dans plusieurs communautés du libre
* Membre de l'APRIL

---

### Déroulement

1. Présentation des entreprises concernées
2. La méthodologie
3. Les outils utilisés
4. Les tests
5. Travail fourni
6. L'après stage
7. Conclusion

---

### Les entreprises concernées

* Le CETIC
* SAM-Drive

---

### Les entreprises concernées
#### CETIC

<span style="font-size: 70%;">
 C<span class="fragment">entre d'</span>E<span class="fragment">xcellence en </span>T<span class="fragment">echnologies de l'</span>I<span class="fragment">nformation et de la </span>C<span class="fragment">ommunication</span>
</span>

* Trois départements (SSE, SST & ECS)
* 45 employés
* Aéropôle de Gosselies

---

### Les entreprises concernées
#### SAM-Drive

Service de navettes en Brabant Wallon

* Début en mai 2014
* Récurrentes ou sporadiques
* Navettes parascolaires
* Sorties nocturnes
* Accompagnement des seniors
* Aéroports & déplacements professionnels
* Évènementiel

---

### Méthodologie

* Utilisation d'une méthode Agile
* Capture des exigences
* Code reviewing

---

### Méthodologie
#### Utilisation d'une méthode Agile

* Itérative & Incrémentale
* Réunion régulière avec le client
* Mise en application des valeurs agiles

---

### Méthodologie
#### Capture des exigences

* Se fait par *User Stories*
* Utilisation d'un Kanban board (backlog)
* Comprendre les requirements

---

### Méthodologie
#### Code reviewing

<img src="images/codereview.png" class="left mini" alt="Code review">

* Application de lecture de code croisée
* Valider/Clarifier les requirements
* Amélioration du produit

---

### Les outils utilisés

* Outils architecturaux
* Outils de gestion de source & de projets
* Outils de collaboration
* Langages & Framework


---

### Les outils utilisés
#### Outils architecturaux

| Docker |
| :----: |
| ![Docker](http://pix.toile-libre.org/upload/original/1497537687.png) |
| <span class="microtext">[docker.com](https://www.docker.com/) |

---

### Les outils utilisés
#### Outils de collaboration

|              Slack               |
| :------------------------------: |
| ![Slack](http://pix.toile-libre.org/upload/original/1497537966.png) |
| <span class="microtext">[slack.com](https://slack.com/) |

---

### Les outils utilisés
#### Outil de gestion de code source et de projets

|             Git              |               Gitlab               |               Backlog                |
| :--------------------------: | :--------------------------------: | :----------------------------------: |
| ![Git](http://pix.toile-libre.org/upload/original/1497538005.png) | ![Gitlab](http://pix.toile-libre.org/upload/original/1497538019.png) | ![Backlog](http://pix.toile-libre.org/upload/original/1497538032.png) |
| <span class="microtext">[git-scm.com](https://git-scm.com/)</span> | <span class="microtext">[gitlab.com](https://about.gitlab.com/)</span> | <span class="microtext">/</span> |

---

### Les outils utilisés
#### Backlogging

* Gestion des user stories

---

![Outil de backlogging](images/screens/backlog1.png)

---

### Les outils utilisés
#### Langages & Framework

|                PHP                 |                AngularJS                 |              Ruby              |        Selenium/Cucumber         |
| :--------------------------------: | :--------------------------------------: | :----------------------------: | :------------------------------: |
| ![PHP](http://pix.toile-libre.org/upload/original/1497538061.png) | ![AngularJS](http://pix.toile-libre.org/upload/original/1497538075.png) | ![Ruby](http://pix.toile-libre.org/upload/original/1497538089.png) | ![SC](http://pix.toile-libre.org/upload/original/1497538103.png) |
| <span class="microtext">[php.net](http://php.net/)</span>         | <span class="microtext">[angularjs.org](https://angularjs.org/)</span>  | <span class="microtext">[ruby-lang.org](https://www.ruby-lang.org/fr/)</span>| <span class="microtext">[seleniumhq.org](http://www.seleniumhq.org/), [cucumber.io](https://cucumber.io/)</span>    |



<!-- ![Langages & Framkwork](images/langagesFramework.png) -->

---

### Les tests

Deux types de tests réalisés:

* Intégration
* Acceptance

---

### Les tests d'intégration

* Valident les requirements
* Valident les end-points de l'API
* Écrits en format `YAML`
* Structure établie avec `Finitio`

---

### Les tests d'acceptance

* Valident l'UX/UI
* Utilisent Selenium/Cucumber
* Rédaction en anglais

```
Scenario: Edit a member of my family
    Given I want to modify his info
    Then I end up on the people registering screen with the member info loaded

```
---

### Travail fourni

* Architectural (1)
* Refactoring (3)
* Améliorations visuelles (2)
* Nouvelles fonctionnalités (7)

---

### Travail fourni
#### Quelques tickets

* *Refactoring:* Ordre des évènements
* *Amélioration visuelle:* barre des heures toujours disponible
* *Nouvelle fonctionnalité:* recherche de client(s) dynamique
* *Nouvelle fonctionnalité:* temps d'attente d'un chauffeur

---

### Travail fourni
#### Recherche de client(s) dynamique

![Recherche de clients dynamique](images/screens/rechercheClient.png)

---

### Travail fourni
#### L'interface client

* Partie principale du stage
* Refactoring des pages du frontoffice
* Responsive design
* Backend

---

### Travail fourni
#### L'interface client: Refactoring des pages du frontoffice

* Séparation des couches présentation, métier et persistance
* Simplification du code redondant

---

### Travail fourni
#### L'interface client: Responsive Design

* Réécriture des pages
* Amélioration de la présentation desktop/mobile
* Simplification de certains éléments

---

![Index](images/screens/index.png)
![Mes courses](images/screens/mesCourses.png)

---

### Travail fourni
#### L'interface client: Backend

* Utilisation des fonctions du DAO
* Différence développement et production

---

### L'après stage

* Travail d'été pour financer…
* <span class="fragment">…Master à l'UMons</span>

---

### Conclusion

* Amélioration générale de mes compétences
* Découverte des projets `tests driven`
* Apprentissage de Docker, Angular, Ruby
* Intégration dans une équipe professionnelle

---

## Merci !
##### Liens

* [SAM-Drive](http://www.sam-drive.be): `www.sam-drive.be`
* [CETIC](http://www.cetic.be): `www.cetic.be`
